const mix = require('laravel-mix');

mix
  .sass("src/scss/tiny-flexgrid.scss",  "dist/css")
  .options({
      autoprefixer: {
        options: {
          browsers: [
            'last 6 versions',
          ]
        }
      }
   })
  .setPublicPath('dist')